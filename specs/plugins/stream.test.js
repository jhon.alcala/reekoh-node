/* global describe, it, before, after */
'use strict'

const amqp = require('amqplib')
const Reekoh = require('../../index.js')
const isEqual = require('lodash.isequal')

process.env.PLUGIN_ID = 'plugin1'
process.env.COMMAND_RELAYS = 'demo.relay1'
process.env.OUTPUT_PIPES = 'Op1,Op2'
process.env.LOGGERS = 'logger1,logger2'
process.env.EXCEPTION_LOGGERS = 'exlogger1,exlogger2'
process.env.BROKER = 'amqp://guest:guest@127.0.0.1/reekoh'
process.env.CONFIG = '{"foo": "bar"}'
process.env.OUTPUT_SCHEME = 'MERGE'
process.env.OUTPUT_NAMESPACE = 'result'
process.env.ACCOUNT = 'demo.account'

// plugin clears above process.env values
let BROKER = process.env.BROKER
let ACCOUNT = process.env.ACCOUNT
let PLUGIN_ID = process.env.PLUGIN_ID
let COMMAND_RELAY = 'demo.relay1'

describe('Stream Plugin Test', () => {
  let _conn = null
  let _plugin = null
  let _channel = null

  before('#test init', () => {
    amqp.connect(BROKER).then((conn) => {
      _conn = conn
      return conn.createChannel()
    }).then((channel) => {
      _channel = channel
    }).catch((err) => {
      console.log(err)
    })
  })

  after('#terminate connection', (done) => {
    _conn.close().then(() => {
      _plugin.removeAllListeners()
      done()
    })
  })

  describe('#spawn', () => {
    it('should spawn the class without error', (done) => {
      _plugin = new Reekoh.plugins.Stream()
      _plugin.once('ready', () => {
        done()
      })
    })
  })

  describe('#events', () => {
    it('should receive `command` event', (done) => {
      let dummyData = {
        account: ACCOUNT,
        pluginId: PLUGIN_ID,
        command: 'TEST_CMD',
        sequenceId: Date.now(),
        deviceGroups: '',
        devices: '567827489028375',
        source: '567827489028376'
      }

      _channel.sendToQueue(COMMAND_RELAY, Buffer.from(JSON.stringify(dummyData)))

      // NOTE: command-relay info should exist in mongo else, PE cannot throw the commands
      // NOTE: device '567827489028375' must be in mongo also and it is 'online'

      _plugin.on('command', (data) => {
        delete dummyData.sequenceId
        if (data.command === dummyData.command) {
          done()
        } else {
          done(new Error('received data not matched'))
        }
      })
    })

    it('should receive `sync` event', (done) => {
      let dummyData = { 'operation': 'sync', '_id': 321, 'name': 'device-321' }
      _channel.sendToQueue(PLUGIN_ID, Buffer.from(JSON.stringify(dummyData)))

      _plugin.on('sync', () => {
        done()
      })
    })
  })

  describe('#pipe()', () => {
    it('should throw error if data is empty', (done) => {
      _plugin.pipe('', '').then(() => {
        done(new Error('Reject expected.'))
      }).catch((err) => {
        if (!isEqual(err, new Error('Invalid data received. Data should be an Object and should not be empty.'))) {
          done(new Error('Return value did not match.'))
        } else {
          done()
        }
      })
    })

    let data = {
      foo: 'bar',
      rkhDeviceInfo: {
        account: ACCOUNT,
        device: {
          _id: '567827489028375'
        }
      }
    }

    it('should publish data to output pipes', (done) => {
      _plugin.pipe(data, '').then(() => {
        done()
      }).catch((err) => {
        done(err)
      })
    })

    it('should publish data to sanitizer', (done) => {
      _plugin.pipe(data, 'seq123').then(() => {
        done()
      }).catch((err) => {
        done(err)
      })
    })
  })

  describe('#sendCommandResponse()', () => {
    it('should throw error if commandId is empty', (done) => {
      _plugin.sendCommandResponse('', 'response').then(() => {
        done(new Error('reject expected'))
      }).catch((err) => {
        if (!isEqual(new Error('Kindly specify the command id'), err)) {
          done(new Error('Return value did not match.'))
        } else {
          done()
        }
      })
    })

    it('should publish message to cmd.responses', (done) => {
      _plugin.sendCommandResponse('command1', 'sample response').then(() => {
        done()
      }).catch(done)
    })
  })

  describe('#syncDevice()', () => {
    it('should throw error if deviceInfo is empty', (done) => {
      _plugin.syncDevice('', []).then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device information/details')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if deviceInfo is not a valid object', (done) => {
      _plugin.syncDevice('{}', []).then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device information/details')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if deviceInfo doesnt have `_id` or `id` property', (done) => {
      _plugin.syncDevice({}, []).then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid id for the device')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if deviceInfo doesnt have `name` property', (done) => {
      _plugin.syncDevice({ _id: '123' }, []).then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid name for the device')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should publish sync msg to queue', (done) => {
      _plugin.syncDevice({ _id: '123', name: 'foo' }, []).then(() => {
        done()
      }).catch(done)
    })
  })

  describe('#requestDeviceInfo()', function () {
    this.timeout(5000)
    it('should throw error if deviceId is empty', (done) => {
      _plugin.requestDeviceInfo('').then(() => {
        // noop!
      }).catch((err) => {
        if (!isEqual(err, new Error('Kindly specify a valid device identifier'))) {
          done(new Error('Return value did not match.'))
        } else {
          done()
        }
      })
    })
    // xxx
    it('should request device info', function (done) {
      _plugin.requestDeviceInfo('567827489028375').then(() => {
        done()
      }).catch((err) => {
        done(err)
      })
    })
  })

  describe('#removeDevice()', () => {
    it('should throw error if deviceId is empty', (done) => {
      _plugin.removeDevice('').then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device identifier')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if deviceId is not a string', (done) => {
      _plugin.removeDevice(123).then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device identifier')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should publish remove msg to queue', (done) => {
      _plugin.removeDevice('123').then(() => {
        setTimeout(done, 500)
      }).catch(done)
    })
  })

  describe('#notifyConnection()', () => {
    it('should throw error if deviceId is empty', (done) => {
      _plugin.notifyConnection('').then(() => {
        done(new Error('Reject expected.'))
      }).catch((err) => {
        if (!isEqual(new Error('Kindly specify a valid device identifier'), err)) {
          done(new Error('Return value did not match.'))
        } else {
          done()
        }
      })
    })

    it('should publish a message to device', (done) => {
      _plugin.notifyConnection('test').then(() => {
        done()
      }).catch((err) => {
        done(err)
      })
    })
  })

  describe('#notifyDisconnection()', () => {
    it('should throw error if deviceId is empty', (done) => {
      _plugin.notifyDisconnection('').then(() => {
        done(new Error('Reject expected.'))
      }).catch((err) => {
        if (!isEqual(new Error('Kindly specify a valid device identifier'), err)) {
          done(new Error('Return value did not match.'))
        } else {
          done()
        }
      })
    })

    it('should publish a message to device', (done) => {
      _plugin.notifyDisconnection('test').then(() => {
        done()
      }).catch((err) => {
        done(err)
      })
    })
  })

  describe('#setDeviceState()', () => {
    it('should throw error if deviceId is empty', function (done) {
      this.timeout(5000)

      _plugin.setDeviceState('', '').then(() => {
        done(new Error('Reject expected.'))
      }).catch(err => {
        if (!isEqual(new Error('Kindly specify a valid device identifier'), err)) {
          done(new Error('Return value did not match.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if state is empty', function (done) {
      this.timeout(5000)

      _plugin.setDeviceState('test').then(() => {
        done(new Error('Reject expected.'))
      }).catch((err) => {
        if (!isEqual(err, new Error('Kindly specify the device state'))) {
          done(new Error('Return value did not match.'))
        } else {
          done()
        }
      })
    })

    it('should publish state msg to queue', function (done) {
      this.timeout(10000)

      _plugin.setDeviceState('567827489028375', 'bar').then(() => {
        done()
      }).catch((err) => {
        done(err)
      })
    })
  })

  describe('#setState()', function () {
    it('should throw error if state is empty', (done) => {
      _plugin.setState(null).then(() => {
        done(new Error('Reject expected.'))
      }).catch((err) => {
        if (!isEqual(new Error('Please specify a valid state to set.'), err)) {
          done(new Error('Return value did not match.'))
        } else {
          done()
        }
      })
    })

    it('should publish state plugin.state to queue', (done) => {
      _plugin.setState(JSON.stringify({ lastSyncData: Date.now() })).then(() => {
        done()
      }).catch(done)
    })
  })

  describe('#getState()', function () {
    this.timeout(8000)

    it('should request plugin state', (done) => {
      _plugin.getState().then(() => {
        done()
      }).catch((err) => {
        done(err)
      })
    })
  })

  describe('#setDeviceLocation()', () => {
    it('should throw error if deviceId is empty', (done) => {
      _plugin.setDeviceLocation('', '', '').then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device identifier')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if deviceId is not a string', (done) => {
      _plugin.setDeviceLocation(123, '').then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device identifier')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if lat is not valid', (done) => {
      _plugin.setDeviceLocation('test', 'foo').then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, `Kindly specify a valid 'lat, long' coordinates`)) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if long is not valid', (done) => {
      _plugin.setDeviceLocation('test', 1.23232, 'bar').then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, `Kindly specify a valid 'lat, long' coordinates`)) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it(`should publish 'device.location' msg to queue`, (done) => {
      _plugin.setDeviceLocation('567827489028375', 14.5831, 120.9794).then(() => {
        done()
      }).catch(done)
    })
  })

  describe('#logging', () => {
    it('should send a log to logger queues', (done) => {
      _plugin.log('dummy log data').then(() => {
        done()
      }).catch(() => {
        done(new Error('send using logger fail.'))
      })
    })

    it('should send an exception log to exception logger queues', (done) => {
      _plugin.logException(new Error('tests')).then(() => {
        done()
      }).catch(() => {
        done(new Error('send using exception logger fail.'))
      })
    })
  })
})
