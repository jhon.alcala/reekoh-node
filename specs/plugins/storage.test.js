/* global describe, it, before, after */

'use strict'

let _conn = null
let _plugin = null
let _channel = null

let amqp = require('amqplib')
let reekoh = require('../../index')
let isEqual = require('lodash.isequal')

// preserving.. plugin clears env after init
const ACCOUNT = 'demo.account'
const INPUT_PIPE = 'demo.storage'
const BROKER = 'amqp://guest:guest@127.0.0.1/reekoh'

describe('Storage Plugin Test', () => {
  before('#test init', (done) => {
    process.env.LOGGERS = ''
    process.env.CONFIG = '{}'
    process.env.EXCEPTION_LOGGERS = ''

    process.env.BROKER = BROKER
    process.env.ACCOUNT = ACCOUNT
    process.env.INPUT_PIPE = INPUT_PIPE

    amqp.connect(BROKER).then((conn) => {
      _conn = conn
      return conn.createChannel()
    }).then((channel) => {
      _channel = channel
      done()
    }).catch((err) => {
      console.log(err)
      done(err)
    })
  })

  after('terminate connection', () => {
    _conn.close()
  })

  describe('#spawn', () => {
    it('should spawn the class without error', (done) => {
      try {
        _plugin = new reekoh.plugins.Storage()
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  describe('#events', () => {
    it('should rcv `ready` event', (done) => {
      _plugin.once('ready', () => {
        done()
      })
    })

    it('should rcv `data` event', (done) => {
      let dummyData = { 'foo': 'bar' }
      _channel.sendToQueue(INPUT_PIPE, Buffer.from(JSON.stringify(dummyData)))

      _plugin.on('data', (data) => {
        if (!isEqual(dummyData, data)) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })
  })

  describe('#setState()', () => {
    it('should throw error if state is empty', (done) => {
      _plugin.setState(undefined).then(() => {
        done(new Error('Expecting rejection. Check your test data.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Please specify a valid state to set.')) {
          done(new Error('Return value did not match.'))
        } else {
          done()
        }
      })
    })

    it('should publish state msg to queue', (done) => {
      _plugin
        .setState(JSON.stringify({ lastSyncData: Date.now() }))
        .then(done)
        .catch(done)
    })
  })

  describe('#getState()', function () {
    this.timeout(8000)

    it('should request plugin state', (done) => {
      _plugin.getState().then(() => {
        done()
      }).catch((err) => {
        done(err)
      })
    })
  })

  describe('#log()', () => {
    it('should throw error if logData is empty', (done) => {
      _plugin.log('').then(() => {
        done(new Error('Expecting rejection. Check your test data.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Please specify a data to log.')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should send a log to logger queues', (done) => {
      _plugin.log('dummy log data').then(() => {
        done()
      }).catch(done)
    })
  })

  describe('#logException()', () => {
    it('should throw error if param is not an Error instance', (done) => {
      _plugin.logException('').then(() => {
        done(new Error('Expecting rejection. Check your test data.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Please specify a valid error to log.')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })
    it('should send an exception log to exception logger queues', (done) => {
      _plugin.logException(new Error('test')).then(() => {
        done()
      }).catch(done)
    })
  })
})
