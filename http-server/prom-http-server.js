'use strict'
const client = require('prom-client')
const registry = new client.Registry()
const bodyParser = require('body-parser')
const express = require('express')
const get = require('lodash.get')
let app = express()

let latency = []
let mem = []
let cpu = []
let memMb = []

let avg = (data) => {
  if (data.length < 1) return 0
  return (data.reduce((a, b) => { return a + b })) / data.length
}

const errorTotal = new client.Counter({
  name: 'errors',
  help: 'total_number_of_errors',
  registers: [registry]
})

const dataRequest = new client.Counter({
  name: 'requests',
  help: 'total_number_of_requests',
  registers: [registry]
})

const latencyGauge = new client.Gauge({
  name: 'latency',
  help: 'average_latency_in_ms',
  registers: [registry]
})

const memGauge = new client.Gauge({
  name: 'memory_usage_percent',
  help: 'average_memory_usage_percent',
  registers: [registry]
})

const memGaugeMb = new client.Gauge({
  name: 'memory_usage_mb',
  help: 'average_memory_usage_mb',
  registers: [registry]
})

const cpuGauge = new client.Gauge({
  name: 'cpu_usage',
  help: 'average_cpu_usage',
  registers: [registry]
})

app.use(bodyParser.json())

app.post('/error', (req, res) => {
  errorTotal.inc()
  res.status(200).end()
})

app.post('/traffic', (req, res) => {
  dataRequest.inc()
  res.status(200).end()
})

app.post('/latency', (req, res) => {
  let latencyValue = get(req.body, 'time')

  if (isNaN(latencyValue)) { latencyValue = 0 }

  latency.push(latencyValue)
  res.status(200).end()
})

app.post('/saturation', (req, res) => {
  let memValue = get(req.body, 'mem')
  let cpuValue = get(req.body, 'cpu')
  let memMbValue = get(req.body, 'memMb')

  if (isNaN(memValue)) { memValue = 0 }
  if (isNaN(cpuValue)) { cpuValue = 0 }
  if (isNaN(memMbValue)) { memMbValue = 0 }

  mem.push(memValue)
  cpu.push(cpuValue)
  memMb.push(memMbValue)
  res.status(200).end()
})

app.get('/metrics', (req, res) => {
  latencyGauge.set(avg(latency))
  memGauge.set(avg(mem))
  memGaugeMb.set(avg(memMb))
  cpuGauge.set(avg(cpu))

  res.end(registry.metrics())

  mem = []
  cpu = []
  latency = []
  memMb = []

  dataRequest.reset()
  errorTotal.reset()
  latencyGauge.reset()
  memGauge.reset()
  memGaugeMb.reset()
  cpuGauge.reset()
})

app.listen(9091, () => {
  console.log('Server is running in 9091')
})
