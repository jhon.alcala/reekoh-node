'use strict'

module.exports = {
  plugins: require('./lib/plugins'),
  logger: require('./lib/reekoh.logger')
}
