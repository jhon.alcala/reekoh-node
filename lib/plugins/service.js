'use strict'

const uuid = require('uuid/v4')
const BPromise = require('bluebird')
const safeParse = BPromise.method(JSON.parse)

const isNil = require('lodash.isnil')
const isEmpty = require('lodash.isempty')
const isError = require('lodash.iserror')
const isString = require('lodash.isstring')
const isPlainObject = require('lodash.isplainobject')

const Prom = require('../prometheus-logger')
const LatencyMonitor = require('../prometheus-latency')
const Broker = require('../broker.lib')

class Service extends LatencyMonitor {
  constructor () {
    super()

    const BROKER = process.env.BROKER
    const ACCOUNT = process.env.ACCOUNT
    const INPUT_PIPE = process.env.INPUT_PIPE

    const OUTPUT_PIPES = `${process.env.OUTPUT_PIPES || ''}`.split(',').filter(Boolean)
    const OUTPUT_SCHEME = `${process.env.OUTPUT_SCHEME || 'MERGE'}`.toUpperCase()
    const OUTPUT_NAMESPACE = `${process.env.OUTPUT_NAMESPACE || 'result'}`

    const LOGGERS = `${process.env.LOGGERS || ''}`.split(',').filter(Boolean)
    const EXCEPTION_LOGGERS = `${process.env.EXCEPTION_LOGGERS || ''}`.split(',').filter(Boolean)

    let _broker = new Broker()
    let _prometheus = new Prom()

    process.env.INPUT_PIPE = undefined
    process.env.OUTPUT_PIPES = undefined
    process.env.LOGGERS = undefined
    process.env.EXCEPTION_LOGGERS = undefined
    process.env.ACCOUNT = undefined
    process.env.BROKER = undefined
    process.env.OUTPUT_SCHEME = undefined
    process.env.OUTPUT_NAMESPACE = undefined

    this.pipe = (data, result) => {
      if (isEmpty(data) || isEmpty(result)) return BPromise.reject(new Error('Please specify the original data and the result.'))

      if (!isPlainObject(data)) return BPromise.reject(new Error('Data should be a valid JSON object.'))

      let sendResult = (outputData) => {
        return BPromise.each(OUTPUT_PIPES, (outputPipe) => {
          return _broker.queues[outputPipe].publish(outputData)
        })
      }

      if (OUTPUT_SCHEME === 'MERGE') {
        if (!isPlainObject(result)) result = { result }
        return sendResult(Object.assign(data, result))
      } else if (OUTPUT_SCHEME === 'NAMESPACE') {
        return sendResult({ [OUTPUT_NAMESPACE]: result })
      } else if (OUTPUT_SCHEME === 'RESULT') {
        if (!isPlainObject(result)) result = { result }
        return sendResult(result)
      }
    }

    this.setState = (state) => {
      if (isNil(state) || (isString(state) && isEmpty(state))) {
        return BPromise.reject(new Error(`Please specify a valid state to set.`))
      }

      return _broker.queues['plugin.state'].publish({
        state: state,
        plugin: INPUT_PIPE
      })
    }

    this.getState = () => {
      return new BPromise((resolve, reject) => {
        let requestId = uuid()
        _broker.rpcs['plugin.state.rpc'].once(requestId, (state) => {
          resolve(state.content.toString())
        })
        _broker.rpcs['plugin.state.rpc'].publish(requestId, {
          plugin: INPUT_PIPE
        }).catch(reject)
      }).timeout(10000, 'Request for plugin state has timed out.')
    }

    this.log = (logData) => {
      if (isEmpty(logData)) return BPromise.reject(new Error(`Please specify a data to log.`))
      if (!isPlainObject(logData) && !isString(logData)) return BPromise.reject(new Error('Log data must be a string or object'))

      return BPromise.all([
        BPromise.each(LOGGERS, logger => {
          return _broker.queues[logger].publish(logData)
        }),
        _broker.queues['logs'].publish({
          account: ACCOUNT,
          pluginId: INPUT_PIPE,
          type: 'Service',
          data: logData
        })
      ])
    }

    this.logException = (err) => {
      _prometheus.error()
      if (!isError(err)) return BPromise.reject(new Error('Please specify a valid error to log.'))

      let errData = {
        name: err.name,
        message: err.message,
        stack: err.stack
      }

      return BPromise.all([
        BPromise.each(EXCEPTION_LOGGERS, exceptionLogger => {
          return _broker.queues[exceptionLogger].publish(errData)
        }),
        _broker.queues['exceptions'].publish({
          account: ACCOUNT,
          pluginId: INPUT_PIPE,
          type: 'Service',
          data: errData
        })
      ])
    }

    safeParse(process.env.CONFIG || '{}').then(config => {
      this.config = config
      process.env.CONFIG = undefined

      return BPromise.resolve()
    }).then(() => {
      return _broker.connect(BROKER)
    }).then(() => {
      let queueIds = [INPUT_PIPE, 'logs', 'exceptions', 'plugin.state']
        .concat(OUTPUT_PIPES)
        .concat(LOGGERS)
        .concat(EXCEPTION_LOGGERS)

      return BPromise.each(queueIds, queueId => {
        return _broker.createQueue(queueId)
      })
    }).then(() => {
      return _broker.queues[INPUT_PIPE].consume((msg) => {
        safeParse(msg.content.toString() || '{}').then(data => {
          this.emit('data', data)
        }).catch(err => {
          console.error(err)
        })
      })
    }).then(() => {
      return _broker.createRPC('client', 'plugin.state.rpc').then((queue) => {
        return queue.consume()
      })
    }).then(() => {
      process.nextTick(() => {
        require('../../http-server/prom-http-server')
        this.emit('ready')
      })

      return BPromise.resolve()
    }).catch(err => {
      console.error(err)
      throw err
    })
  }
}

module.exports = Service
