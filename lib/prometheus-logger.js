'use strict'

let rp = require('request-promise')
let PORT = 9091
let options

class prometheus {
  error () {
    options = {
      method: 'POST',
      uri: `http://localhost:${PORT}/error`,
      body: {},
      json: true
    }
    rp(options)
      .catch(console.log)
  }
}

module.exports = prometheus
